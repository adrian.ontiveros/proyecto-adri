<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <xsl:for-each select="Videojuegos/Videojuego">
            <article>
                <img alt="" src="{concat('media/',imagen)}"></img>
                <h4 class="gametitle"><xsl:value-of select="nombre"></xsl:value-of></h4>
                <p><xsl:value-of select="descripcion"></xsl:value-of></p>
                <span><a><xsl:attribute name="href"><xsl:value-of select="link"></xsl:value-of></xsl:attribute>más info</a></span>
            </article>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>