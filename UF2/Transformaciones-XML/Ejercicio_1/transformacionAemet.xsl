<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <style>img{width: 50px; height: 50px;} table, td{border: 2px solid blue;}</style>
            <body>
                <h1>AEMET</h1>
                <table>
                    <tr>
                        <th>Fecha</th><th>Máxima</th><th>Mínima</th><th>Predicción</th>
                    </tr>
                    <xsl:for-each select="root/prediccion/dia">
                        <xsl:sort select="temperatura/maxima" order="descending"></xsl:sort>
                        <tr>
                            <td><xsl:value-of select="@fecha"/></td>
                            <td><xsl:value-of select="temperatura/maxima"/></td>
                            <td><xsl:value-of select="temperatura/minima"/></td>
                            <td><img src="{concat('imagenes/',estado_cielo[@periodo = '00-24']/@descripcion)}.png"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>