<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <style>table, td{border: 1px solid black;} th{background-color: lightgreen;} img{height: 60px; width: 60px;}</style>
            <body>
                <h1>M04 - Notas</h1>
                <table>
                    <tr>
                        <th>Foto</th><th>Nombre</th><th>Apellidos</th><th>Telefono</th><th>Nota Practica</th><th>Nota Examen</th><th>Nota Total</th>
                    </tr>
                    <xsl:for-each select="evaluacion/alumno">
                        <xsl:sort select="apellidos" order="ascending"/>
                        <tr>
                            <xsl:choose>
                                <xsl:when test="imagenPerfil[@imagen] != ''">
                                    <td><img src="imagenes/default.jpg"/></td>
                                </xsl:when>
                                <!--<xsl:otherwise test="@imagen = ''">
                                    <td><xsl:value-of select="{concat('imagenes/', imagenPerfil[@imagen])}.jpg"/></td>
                                </xsl:otherwise>-->
                            </xsl:choose>
                            <td><xsl:value-of select="nombre"/></td>
                            <td><xsl:value-of select="apellidos"/></td>
                            <td><xsl:value-of select="telefono"/></td>
                            <td><xsl:value-of select="notas/practicas"/></td>
                            <td><xsl:value-of select="notas/examen"/></td>
                            <xsl:choose>
                                <xsl:when test="((notas/examen + notas/practicas)div 2) &lt; 5">
                                    <td style="background-color: rgb(226, 62, 62);"><xsl:value-of select="((notas/examen + notas/practicas)div 2)"/></td>
                                </xsl:when>
                                <xsl:when test="((notas/examen + notas/practicas)div 2) &gt;= 8">
                                    <td style="background-color: lightblue;"><xsl:value-of select="((notas/examen + notas/practicas)div 2)"/></td>
                                </xsl:when>
                                <xsl:otherwise><td style="background-color: lightgray;"><xsl:value-of select="((notas/examen + notas/practicas)div 2)"/></td></xsl:otherwise>
                            </xsl:choose>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>